# Linked-Data Inspector

This dead-simple WebExtensions shows an icon when a supported linked data format is present on the current website and allow for data inspection on click. Built with react-json-inspector and webextension-toolbox.

## Install

To install depedencies, simply run `npm install` or `yarn`

## Development

To run on your favorite browser, run `npm run dev your-browser`. If you are not using firefox, you may need to setup addon development mode.

    npm run dev chrome
    npm run dev firefox
    npm run dev opera
    npm run dev edge

## Build

For building and packaging for distribution, run:

    npm run build chrome
    npm run build firefox
    npm run build opera
    npm run build edge

## Future work

- [ ] Improve linked data support:
    - [x] Use [web-auto-extractor](https://www.npmjs.com/package/web-auto-extractor) for adding support for multiple formats (JSON-LD, RDFa, Microdata & some meta tags)
- [ ] Improve design
- [ ] Informative tooltip & icon
    - [ ] Change icon depending on data (count badge?)
- [ ] Improve popup
    - [ ] Allow data download?
    - [ ] Normalize RDF data in a single property
    - [ ] Normalize microformats data
- [ ] Implement opt-in local collection of data on RDF usage
    - [ ] Figure out a visualization

## License

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)