let listener = null;
const tabs = new Map();

async function addDataListener(fn) {
  const tab = (await browser.tabs.query({ active: true, currentWindow: true }))[0];
  const tabId = tab.id;

  listener = fn;

  console.log("[indie.background] listener added", { tabId, tab })

  const data = tabs.get(tabId);
  console.log("[indie.background] data for tab", data);
  if (data && listener) {
    console.log("[indie.background] addDataListener: sending data to popup");
    fn(data.data);
  }
}

function removeDataListener(fn) {
  listeners = listeners.filter(foo => fn !== foo);
}

window.addDataListener = addDataListener;
window.removeDataListener = removeDataListener;

browser.runtime.onInstalled.addListener((details) => {
  console.log('[indie.background] previousVersion', details.previousVersion)
});

function setBadge(tabId) {
  if (tabs.has(tabId)) {
    const countFormats = Object.values(tabs.get(tabId).uses).filter(v => v).length;
    browser.browserAction.setBadgeText({ text: `${countFormats}`, tabId });
  }
}

browser.tabs.onUpdated.addListener(setBadge);

browser.tabs.onRemoved.addListener((tabId) => {
  if (tabs.has(tabId)) {
    tabs.delete(tabId);
  }
});

browser.runtime.onMessage.addListener(async (data, sender, sendResponse) => {
  console.log("[indie.background] received data from content", { tab: sender.tab.id });
  sendResponse(null);

  const tabId = sender.tab.id;
  tabs.set(tabId, data);
  setBadge(tabId);

  if (data && listener && tabId === await browser.tabs.getCurrent().id) {
    console.log("[indie.background] onMessage: sending data to popup");
    listener(data.data);
  }
});
