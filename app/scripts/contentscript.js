function parseAndSendData() {
    const microformat = require("microformat-shiv");
    const extractor = require("web-auto-extractor").default;

    const rdfData = extractor().parse(document.querySelector("html").outerHTML);
    const microformatData = microformat.get();

    const data = {
        uses: {
            rdfa: Object.keys(rdfData.rdfa).length > 0,
            jsonld: Object.keys(rdfData.jsonld).length > 0,
            microdata: Object.keys(rdfData.microdata).length > 0,
            microformats: microformatData.items && microformatData.items.length > 0 || false,
        },
        data: {
            others: {
                rels: microformatData.rels,
                metatags: rdfData.metatags,
                "rel-urls": microformatData["rel-urls"],
            },
            microformats: microformatData.items,
            rdf: [rdfData.jsonld, rdfData.microdata, rdfData.rdfa].reduce((rdfAcc, format) => {
                Object.entries(format).forEach(([kind, value]) => {
                    rdfAcc[kind] = (rdfAcc[kind] || []).concat(value);
                });
                return rdfAcc;
            }, {}),
        },
    };

    try {
        console.log("[indie.content] parsed data", data);

        browser.runtime.sendMessage(data)
            .then((...args) => console.log("[indie.content] response received", ...args))
            .catch((err) => console.error("[indie.content] response is error", err));

        console.log("[indie.content] sent data to background");
    } catch (err) {
        console.error("[indie.content]", err);
    }
}

parseAndSendData();
