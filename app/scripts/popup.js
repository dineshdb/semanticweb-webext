// @jsx React.createElement
// @jsxFrag React.createElement
import { render } from "react-dom";
import * as React from "react";
import * as JSONTree from "react-json-inspector";

function generatePopup(data) {
    const main = document.getElementById("main");

    console.log("[indie.popup] rocking", data);

    render((
        <React.Fragment>
            <h1>Linked-Data</h1>
            <hr/>
            <h2>Microdata</h2>
            <JSONTree data={data}/>
        </React.Fragment>
    ), main);
}

console.log("[indie.popup] will get background page");
browser.runtime.getBackgroundPage().then(page => {
    console.log("[indie.popup] got background page");
    page.addDataListener(generatePopup);
});
